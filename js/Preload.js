var Thrust = Thrust || {};

// Load the game assets
Thrust.Preload = function(){};

Thrust.Preload.prototype = {
  preload: function() {
    // Show loading screen
    this.splash = this.add.sprite(
      this.game.world.centerX,
      this.game.world.centerY,
      'logo'
    );
    this.splash.anchor.setTo(0.5);

    this.preloadBar = this.add.sprite(
      this.game.world.centerX,
      this.game.world.centerY + 128,
      'preloadbar'
    );
    this.preloadBar.anchor.setTo(0.5);

    this.load.setPreloadSprite(this.preloadBar);

    // Load game assets
    this.load.image('space',            'assets/images/space.png');
    this.load.image('meteorBig',        'assets/images/meteorBig.png');
    this.load.image('meteorSmall',      'assets/images/meteorSmall.png');
    this.load.image('playerParticle',   'assets/images/player-particle.png');
    this.load.audio('collect',          'assets/audio/collect.ogg');
    this.load.audio('explosion',        'assets/audio/explosion.ogg');
    this.load.audio('bgmusic', [
      'assets/music/quisper.mp3',
      'assets/music/quisper.ogg'
    ]);

    this.load.spritesheet('playerShip', 'assets/images/player.png', 35, 35);
    this.load.spritesheet('power',      'assets/images/power.png',  12, 12);
  },
  create: function() {
    this.state.start('MainMenu');
  }
};
