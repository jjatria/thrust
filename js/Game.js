var Thrust = Thrust || {};

// Title screen
Thrust.Game = function(){};

Thrust.Game.prototype = {
  create: function() {
    // Set world dimensions
    this.game.world.setBounds(0, 0, 1920, 1920);

    // Background
    this.background = this.game.add.tileSprite(
      0, 0,
      this.game.world.width,
      this.game.world.height,
      'space'
    );

    // Create player
    this.player = this.game.add.sprite(
      this.game.world.centerX,
      this.game.world.centerY,
      'playerShip'
    );
    this.player.anchor.setTo(0.5);
    this.player.animations.add('thrust', [1, 2], 2, true);

    this.playerVector = this.game.add.graphics(
      this.player.x,
      this.player.y
    );
    this.playerVector.lineStyle(2, 0xffd900);
    this.playerVector.line = new Phaser.Line(
      this.player.x,
      this.player.y,
      this.player.x,
      this.player.y
    );
    // Player is drawn on top of vector
    this.game.world.bringToTop(this.player);

    // Initialise player score
    this.playerScore = 0;

    // Enable player physics
    this.game.physics.arcade.enable(this.player);
    this.player.body.collideWorldBounds = true;

    // The camera will follow the player in the world
    this.game.camera.follow(this.player);

    // Generate game elements
    this.generateAsteriods();

    // Sounds
    this.explosionSound = this.game.add.audio('explosion');

    // Hotkeys
    this.debug = 0;
    this.debugKey = this.game.input.keyboard.addKey(Phaser.Keyboard.D);
    this.debugKey.onDown.add(this.debugCycle, this);

    this.muteKey = this.game.input.keyboard.addKey(Phaser.Keyboard.M);
    this.muteKey.onDown.add(this.mute, this);

  },
  update: function() {

    this.game.input.enabled = this.game.input.activePointer.withinGame;
    if (this.game.input.activePointer.leftButton.isDown) {
      this.game.physics.arcade.accelerationFromRotation(
        this.player.rotation,
        this.game.physics.arcade.distanceToPointer(this.player),
        this.player.body.acceleration
      );
      this.player.animations.play('thrust');

    } else {
      this.player.body.acceleration.set(0);
      this.player.animations.stop();
      this.player.frame = 0;
    }
    // Collision between player and asteroids
    this.game.physics.arcade.collide(
      this.player,
      this.asteroids,
      this.hitAsteroid,
      null,
      this
    );

  },
  debugCycle: function() {
    this.debug++;
    if (this.debug > 2) {
      this.debug = 0;
    }
    console.log("Debug level: ", this.debug);
  },
  mute: function() {
    this.sound.mute = !this.sound.mute;
  },
  generateAsteriods: function() {
    this.asteroids = this.game.add.group();

    // Enable physics in asteroids
    this.asteroids.enableBody = true;

    // Phaser's random number generator
    var numAsteroids = this.game.rnd.integerInRange(10, 20);
    var asteroid;

    var asteroidSprites = ['meteorBig', 'meteorSmall'];
    for (var i = 0; i < numAsteroids; i++) {
      // Add sprite
      asteroid = this.asteroids.create(
        this.game.world.randomX,
        this.game.world.randomY,
        asteroidSprites[this.game.rnd.integerInRange(0, 1)]
      );
      asteroid.scale.setTo(this.game.rnd.integerInRange(-10, 10) / 10);
      asteroid.anchor.setTo(0.5);
      asteroid.body.angularVelocity = this.game.rnd.integerInRange(-10, 10);

      // Physics properties
      asteroid.body.velocity.x = this.game.rnd.integerInRange(-20, 20);
      asteroid.body.velocity.y = this.game.rnd.integerInRange(-20, 20);
      asteroid.body.immovable = true;
      asteroid.body.collideWorldBounds = true;
    }
  },
  hitAsteroid: function(player, asteroid) {
    // Play explosion sound
    this.explosionSound.play();

    // Make the player explode
    var emitter = this.game.add.emitter(this.player.x, this.player.y, 100);
    emitter.makeParticles('playerParticle');
    emitter.minParticleSpeed.setTo(-200, -200);
    emitter.maxParticleSpeed.setTo(200, 200);
    emitter.gravity = 0;
    emitter.start(true, 1000, null, 100);
    this.player.kill();

    this.game.time.events.add(800, this.gameOver, this);
  },
  gameOver: function() {
    // Pass it the score as a parameter
    this.game.state.start('MainMenu', true, false, this.playerScore);
  },
  render: function() {
    // Ship faces cursor
    this.player.rotation = this.game.physics.arcade.angleToPointer(
        this.player
    );

    // Show player vector
    this.playerVector.line.fromAngle(
      this.player.x,
      this.player.y,
      this.player.body.angle,
      this.player.body.speed
    );
    this.playerVector.graphicsData = [];
    this.playerVector.reset(this.player.x, this.player.y);
    this.playerVector.moveTo(0, 0);
    this.playerVector.lineTo(
      this.playerVector.line.end.x - this.player.x, 
      this.playerVector.line.end.y - this.player.y
    );


    switch (this.debug) {
      case 2:
        this.game.debug.body(this.player);
        for (var i=0; i < this.asteroids.length; i++) {
          this.game.debug.body(this.asteroids.children[i]);
        }
      case 1:
        this.game.debug.bodyInfo(this.player, 0, 0, '#fff');
        break;
      default:
        break;
    }
  },
};
