var Thrust = Thrust || {};

Thrust.Boot = function(){};

// Set game configuration and loading the assets
// for the loading screen
Thrust.Boot.prototype = {
  preload: function() {
    // Assets we'll use in the loading screen
    this.load.image('logo',       'assets/images/logo.png');
    this.load.image('preloadbar', 'assets/images/preloader-bar.png');
  },
  create: function() {
    // Loading screen will have a white background
    this.game.stage.backgroundColor = '#000';

    // Scaling options
    this.scale.scaleMode = Phaser.ScaleManager.RESIZE;
    this.scale.minWidth  = 240;
    this.scale.minHeight = 170;
    this.scale.maxWidth  = 2880;
    this.scale.maxHeight = 1920;

    // Have the game centered horizontally
    this.scale.pageAlignHorizontally = true;

    // Physics system for movement
    this.game.physics.startSystem(Phaser.Physics.ARCADE);

    this.state.start('Preload');
  }
};
