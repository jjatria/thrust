var Thrust = Thrust || {};

// Title screen
Thrust.MainMenu = function(){};

Thrust.MainMenu.prototype = {
  init: function(score) {
    var score = score || 0;
    this.highestScore = this.highestScore || 0;

    this.highestScore = Math.max(score, this.highestScore);
  },
  create: function() {
    // Show the space tile, repeated
    this.background = this.game.add.tileSprite(
      0,
      0,
      this.game.width,
      this.game.height,
      'space'
    );

    this.game.bgmusic = this.game.bgmusic || this.game.add.audio(
      'bgmusic', 0.5, true
    );
    if (this.game.bgmusic.isPlaying) {
      console.log("playing");
    } else {
      console.log("Play");
      this.game.bgmusic.play();
    }

    // Give it horizontal speed
    this.background.autoScroll(-20, 0);

    // Start game text
    var t = this.game.add.text(
      this.game.width  / 2,
      this.game.height / 2,
      "Tap to begin",
      {
        font: "30px Arial",
        fill: "#fff",
        align: "center"
      }
    );
    t.anchor.set(0.5);

    // Highest score
    var h = this.game.add.text(
      this.game.width  / 2,
      this.game.height / 2 + 50,
      "Highest score: " + this.highestScore,
      {
        font: "15px Arial",
        fill: "#fff",
        align: "center"
      }
    );
    h.anchor.set(0.5);
    this.game.state.start('Game');
  },
  update: function() {
    if(this.game.input.activePointer.justPressed()) {
      this.game.state.start('Game');
    }
  }
};
