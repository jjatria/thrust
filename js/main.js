var Thrust = Thrust || {};

Thrust.game = new Phaser.Game(
  window.innerWidth,
  window.innerHeight,
  Phaser.CANVAS,
  ''
);

Thrust.game.state.add('Boot',     Thrust.Boot);
Thrust.game.state.add('Preload',  Thrust.Preload);
Thrust.game.state.add('MainMenu', Thrust.MainMenu);
Thrust.game.state.add('Game',     Thrust.Game);

Thrust.game.state.start('Boot');
